# Revolut Test

This application was developed to show the currency exchanges.

## Architecture

1.  The current architecture uses the [MVVM](https://developer.android.com/topic/libraries/architecture/viewmodel) pattern to handle the business logic and manage views lifecycle.
2.  The REST services are consumed and functions transformations were done with Kotlin [Coroutines](https://developer.android.com/kotlin/coroutines) and [Retrofit](https://github.com/square/retrofit).
3.  To build and take advantage of the dependencies injection pattern the app uses [Koin Framework](https://insert-koin.io/).
4.  Different country options in the main view were shown using the Adapter pattern.
5.  To update the different country fields the app uses [Databinding](https://developer.android.com/topic/libraries/data-binding) to notify other items any data update.
6.  To show the country flag dynamically the app consumes a third [API](https://restcountries.eu) to match the currency with their respective flag.
7.  To upload SVG files correctly the app uses a custom [Glide](https://bumptech.github.io/glide/) configuration.
