package com.tolodev.revoluttest.data.repository

import com.tolodev.revoluttest.api.api.CurrencyApi
import com.tolodev.revoluttest.api.api.FlagApi
import com.tolodev.revoluttest.ui.model.RateItem
import com.tolodev.revoluttest.ui.views.ItemRateView
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

private val BASE_CURRENCY = "EUR"

class CurrencyRepository(
    private val currencyApi: CurrencyApi,
    private val flagApi: FlagApi
) {

    suspend fun getCurrencies(
        baseCurrency: String = BASE_CURRENCY,
        listener: ItemRateView.ItemRateViewListener
    ): List<RateItem> {

        val countryCurrencies = mutableListOf<RateItem>()

        val filterParams = "name;flag;currencies"

        val rateResponse =
            withContext(CoroutineScope(Dispatchers.IO).coroutineContext) {
                currencyApi.getCurrencies(baseCurrency)
            }

        val countryResponse =
            withContext(Dispatchers.IO) {
                flagApi.getFlags(filterParams)
            }

        rateResponse.rates[baseCurrency] = 1.0
        for (rate in rateResponse.rates) {
            for (country in countryResponse) {
                val countryCurrency = country.currencies.find { it.code.equals(rate.key, true) }
                if (countryCurrency != null) {
                    countryCurrencies.add(
                        RateItem(
                            rate.key,
                            rate.value,
                            rate.value,
                            rate.value,
                            country.flagUrl,
                            listener
                        )
                    )
                    break
                }
            }
        }

        return countryCurrencies.toList()
    }
}