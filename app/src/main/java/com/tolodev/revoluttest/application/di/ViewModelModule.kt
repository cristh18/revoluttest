package com.tolodev.revoluttest.application.di

import com.tolodev.revoluttest.ui.viewModels.MainViewModel
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module

val viewModelModule = module(override = true) {
    viewModel { MainViewModel(get()) }
}