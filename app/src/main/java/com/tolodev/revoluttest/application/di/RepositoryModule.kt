package com.tolodev.revoluttest.application.di

import com.tolodev.revoluttest.data.repository.CurrencyRepository
import org.koin.dsl.module

val repositoryModule = module(override = true) {
    single { CurrencyRepository(currencyApi = get(), flagApi = get()) }
}