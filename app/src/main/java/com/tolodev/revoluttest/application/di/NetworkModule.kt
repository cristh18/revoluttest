package com.tolodev.revoluttest.application.di

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import com.tolodev.revoluttest.R
import com.tolodev.revoluttest.api.api.CurrencyApi
import com.tolodev.revoluttest.api.api.FlagApi
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import org.koin.android.ext.koin.androidContext
import org.koin.core.qualifier.named
import org.koin.dsl.module
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

private val REQUEST_TIMEOUT = 60L

val netModule = module(override = true) {

    single<Retrofit>(named("currency_retrofit")) {
        Retrofit.Builder()
            .client(get())
            .baseUrl(androidContext().getString(R.string.base_url))
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .build()
    }

    single<Retrofit>(named("flag_retrofit")) {
        Retrofit.Builder()
            .client(get())
            .baseUrl(androidContext().getString(R.string.countries_url))
            .addConverterFactory(MoshiConverterFactory.create(get()))
            .build()
    }

    fun provideOkHttpClient(): OkHttpClient {
        val httpClient = OkHttpClient.Builder()
            .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
            .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

        val interceptor = HttpLoggingInterceptor()
        interceptor.level = HttpLoggingInterceptor.Level.BODY

        httpClient.addInterceptor(interceptor)

        httpClient.addInterceptor { chain ->
            val original = chain.request()
            val requestBuilder = original.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Content-Type", "application/json")
            requestBuilder.method(chain.request().method, chain.request().body)

            val request = requestBuilder.build()
            chain.proceed(request)
        }
        return httpClient.build()
    }

    fun provideMoshiBuilder(): Moshi {
        return Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single { provideOkHttpClient() }
    single { provideMoshiBuilder() }
    factory { get<Retrofit>(named("currency_retrofit")).create(CurrencyApi::class.java) }
    factory { get<Retrofit>(named("flag_retrofit")).create(FlagApi::class.java) }
}