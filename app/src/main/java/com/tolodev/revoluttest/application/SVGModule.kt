package com.tolodev.revoluttest.application

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.util.Log
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.Registry
import com.bumptech.glide.annotation.GlideModule
import com.bumptech.glide.module.AppGlideModule
import com.caverock.androidsvg.SVG
import com.tolodev.revoluttest.utils.SvgDecoder
import com.tolodev.revoluttest.utils.SvgDrawableTranscoder
import java.io.InputStream

@GlideModule
class SVGModule : AppGlideModule(){

    override fun registerComponents(context: Context, glide: Glide, registry: Registry) {
        registry.append( InputStream::class.java, SVG::class.java, SvgDecoder() )
        registry.register( SVG::class.java, PictureDrawable::class.java, SvgDrawableTranscoder() )
    }

    override fun applyOptions(context: Context, builder: GlideBuilder) {
        builder.setLogLevel(Log.DEBUG)
    }

    override fun isManifestParsingEnabled(): Boolean {
        return false
    }
}

