package com.tolodev.revoluttest.application

import android.app.Application
import com.tolodev.revoluttest.application.di.netModule
import com.tolodev.revoluttest.application.di.repositoryModule
import com.tolodev.revoluttest.application.di.viewModelModule
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.core.logger.Level

class RevolutApplication : Application() {

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@RevolutApplication)
            androidLogger(Level.DEBUG)
            modules(listOf(viewModelModule, repositoryModule, netModule))
        }
    }
}