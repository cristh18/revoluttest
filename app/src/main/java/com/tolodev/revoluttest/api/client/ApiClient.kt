package com.tolodev.revoluttest.api.client

import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit

private val REQUEST_TIMEOUT = 60L

fun getRemoteClient(url: String): Retrofit {
    return Retrofit.Builder()
        .client(getOkHttpClient())
        .baseUrl(url)
        .addConverterFactory(MoshiConverterFactory.create(getMoshiBuilder()))
        .build()
}


private fun getMoshiBuilder(): Moshi {
    return Moshi.Builder()
        .add(KotlinJsonAdapterFactory())
        .build()
}

private fun getOkHttpClient(): OkHttpClient {
    val httpClient = OkHttpClient.Builder()
        .connectTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
        .readTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)
        .writeTimeout(REQUEST_TIMEOUT, TimeUnit.SECONDS)

    val interceptor = HttpLoggingInterceptor()
    interceptor.level = HttpLoggingInterceptor.Level.BODY

    httpClient.addInterceptor(interceptor)

    httpClient.addInterceptor { chain ->
        val original = chain.request()
        val requestBuilder = original.newBuilder()
            .addHeader("Accept", "application/json")
            .addHeader("Content-Type", "application/json")
        requestBuilder.method(chain.request().method, chain.request().body)

        val request = requestBuilder.build()
        chain.proceed(request)
    }
    return httpClient.build()
}