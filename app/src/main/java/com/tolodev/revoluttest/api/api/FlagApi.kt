package com.tolodev.revoluttest.api.api

import com.tolodev.revoluttest.api.models.CountryResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface FlagApi {

    @GET("rest/v2/all")
    suspend fun getFlags(@Query("fields") fields: String): List<CountryResponse>
}