package com.tolodev.revoluttest.api.api

import com.tolodev.revoluttest.api.models.RateResponse
import retrofit2.http.GET
import retrofit2.http.Query

interface CurrencyApi {

    @GET("latest")
    suspend fun getCurrencies(@Query("base") baseCurrency: String): RateResponse
}