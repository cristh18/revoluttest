package com.tolodev.revoluttest.api.models

import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class RateResponse(
    @field:Json(name = "base") val base: String,
    @field:Json(name = "date") val date: String,
    @field:Json(name = "rates") val rates: MutableMap<String, Double>
)

@JsonClass(generateAdapter = true)
data class CountryResponse(
    @field:Json(name = "currencies") val currencies: List<CountryCurrency>,
    @field:Json(name = "flag") val flagUrl: String,
    @field:Json(name = "name") val name: String
)

@JsonClass(generateAdapter = true)
data class CountryCurrency(
    @field:Json(name = "code") val code: String?,
    @field:Json(name = "name") val name: String?,
    @field:Json(name = "symbol") val symbol: String?
)