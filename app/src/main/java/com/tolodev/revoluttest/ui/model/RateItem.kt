package com.tolodev.revoluttest.ui.model

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import androidx.databinding.library.baseAdapters.BR
import com.tolodev.revoluttest.ui.views.ItemRateView

class RateItem(
    val countryCode: String,
    var value: Double,
    var newValue: Double,
    var referenceValue: Double,
    val flagUrl: String,
    val listener: ItemRateView.ItemRateViewListener
) : BaseObservable() {

    @Bindable
    fun getCurrentValue(): String {
        return newValue.toString()
    }

    fun setCurrentValue(currentValue: String) {
        val numericCurrentValue = if (currentValue.isNotBlank()) currentValue.toDouble() else 0.0
        if (newValue != numericCurrentValue) {
            newValue = numericCurrentValue
            notifyPropertyChanged(BR.currentValue)
            listener.onChangedValue(this)
        }
    }

    fun updateWithReference(newValueEntered: Double) {
        newValue = newValueEntered * referenceValue
    }

    fun setCurrentValue() {
        if (newValue != value) {
            value = newValue
            notifyPropertyChanged(BR.currentValue)
        }
    }
}