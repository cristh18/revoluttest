package com.tolodev.revoluttest.ui.viewModels

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.tolodev.revoluttest.R
import com.tolodev.revoluttest.api.client.getRemoteClient
import com.tolodev.revoluttest.api.api.CurrencyApi
import com.tolodev.revoluttest.application.RevolutApplication
import com.tolodev.revoluttest.data.repository.CurrencyRepository
import com.tolodev.revoluttest.ui.model.RateItem
import com.tolodev.revoluttest.ui.views.ItemRateView
import kotlinx.coroutines.*

class MainViewModel(private val currencyRepository: CurrencyRepository) : ViewModel(),
    ItemRateView.ItemRateViewListener {

    private val BASE_CURRENCY = "EUR"

    var thereIsItemInMotion = false

    private val viewModelJob = Job()

    private val viewmodelCoroutineScope = CoroutineScope(Dispatchers.IO + viewModelJob)

    private val homeViewStateLiveData = MutableLiveData<List<RateItem>>()

    private val showError = MutableLiveData<Throwable>()

    private var countryCurrencies = listOf<RateItem>()

    init {
        loadData()
    }

    private fun loadData(baseCurrency: String = BASE_CURRENCY) {
        thereIsItemInMotion = true
        GlobalScope.launch(Dispatchers.Main) {
            getCountryCurrencies(baseCurrency)
        }
    }

    private suspend fun getCountryCurrencies(baseCurrency: String) {
        while (thereIsItemInMotion) {
            val coroutineExceptionHandler =
                CoroutineExceptionHandler { _, exception -> showError.postValue(exception) }

            viewmodelCoroutineScope.launch(coroutineExceptionHandler) {
                val rateItems = currencyRepository.getCurrencies(
                    baseCurrency,
                    this@MainViewModel
                )
                countryCurrencies = rateItems
                countryCurrencies.toMutableList().apply {
                    sortByDescending { it.countryCode.equals(baseCurrency, true) }
                    homeViewStateLiveData.postValue(this)
                }
            }
            delay(1000)
        }
    }

    override fun onCleared() {
        viewmodelCoroutineScope.coroutineContext.cancelChildren()
    }

    override fun onChangedValue(rateItem: RateItem) {
        countryCurrencies.map {
            if (it.countryCode != rateItem.countryCode) {
                it.updateWithReference(rateItem.newValue)
                it.setCurrentValue()
            }
        }
    }

    override fun onEnableAutomaticUpdate(enableAutomaticUpdate: Boolean) {
        thereIsItemInMotion = enableAutomaticUpdate
    }

    fun getCurrenciesByReference(currentCurrency: String) {
        loadData(currentCurrency)
    }

    /**
     * LiveData
     */
    fun getHomeViewStateLiveData(): LiveData<List<RateItem>> = homeViewStateLiveData

    fun showErrorEvent(): LiveData<Throwable> = showError
}