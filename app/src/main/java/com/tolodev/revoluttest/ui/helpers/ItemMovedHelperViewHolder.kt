package com.tolodev.revoluttest.ui.helpers

interface ItemMovedHelperViewHolder {

    fun onItemSelected()

    fun onItemClear()
}