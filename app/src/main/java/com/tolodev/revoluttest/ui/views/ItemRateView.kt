package com.tolodev.revoluttest.ui.views

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.databinding.DataBindingUtil
import com.bumptech.glide.RequestBuilder
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.bumptech.glide.request.RequestOptions
import com.tolodev.revoluttest.R
import com.tolodev.revoluttest.application.GlideApp
import com.tolodev.revoluttest.databinding.ViewItemRateBinding
import com.tolodev.revoluttest.ui.model.RateItem
import com.tolodev.revoluttest.utils.SvgSoftwareLayerSetter

class ItemRateView(context: Context) : FrameLayout(context), View.OnFocusChangeListener {

    private val binding: ViewItemRateBinding =
        DataBindingUtil.inflate(LayoutInflater.from(context), R.layout.view_item_rate, this, true)

    lateinit var rateItem: RateItem

    init {
        layoutParams = LayoutParams(
            ViewGroup.LayoutParams.MATCH_PARENT,
            ViewGroup.LayoutParams.WRAP_CONTENT
        )
        binding.editTextEnterValue.onFocusChangeListener = this
    }

    fun bindView() {
        binding.rate = rateItem
        loadFlagImage()
        binding.root.hasFocus()
    }

    private fun loadFlagImage() {
        val requestBuilder: RequestBuilder<PictureDrawable> = GlideApp.with(this)
            .`as`(PictureDrawable::class.java)
            .apply(RequestOptions.circleCropTransform())
            .transition(withCrossFade())
            .listener(SvgSoftwareLayerSetter())

        val uri: Uri = Uri.parse(rateItem.flagUrl)
        requestBuilder.load(uri)
            .into(binding.imageViewFlag)
    }

    override fun onFocusChange(view: View, isFocused: Boolean) {
        if (view.id == binding.editTextEnterValue.id){
            rateItem.listener.onEnableAutomaticUpdate(isFocused)
        }
    }

    interface ItemRateViewListener {
        fun onChangedValue(rateItem: RateItem)
        fun onEnableAutomaticUpdate(enableAutomaticUpdate: Boolean)
    }
}