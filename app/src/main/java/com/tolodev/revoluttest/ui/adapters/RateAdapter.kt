package com.tolodev.revoluttest.ui.adapters

import android.graphics.Color
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.tolodev.revoluttest.ui.helpers.ItemMovedHelperAdapter
import com.tolodev.revoluttest.ui.helpers.ItemMovedHelperViewHolder
import com.tolodev.revoluttest.ui.model.RateItem
import com.tolodev.revoluttest.ui.views.ItemRateView
import java.util.*

class RateAdapter(private val action: (currency: String) -> Unit) :
    RecyclerView.Adapter<RateAdapter.ItemRateViewHolder>(),
    ItemMovedHelperAdapter {

    private val items = mutableListOf<RateItem>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemRateViewHolder {
        return ItemRateViewHolder(ItemRateView(parent.context))
    }

    override fun onBindViewHolder(holder: ItemRateViewHolder, position: Int) {
        holder.itemRateView.apply {
            rateItem = items[position]
            bindView()
        }
    }

    override fun getItemCount(): Int = items.size

    fun setItems(items: List<RateItem>) {
        this.items.clear()
        this.items.addAll(items)
        notifyDataSetChanged()
    }

    override fun onItemMoved(fromPosition: Int, toPosition: Int) {
        if (fromPosition < toPosition) {
            for (i in fromPosition until toPosition) {
                Collections.swap(items, i, i + 1)
            }
        } else {
            for (i in fromPosition downTo toPosition + 1) {
                Collections.swap(items, i, i - 1)
            }
        }
        notifyItemMoved(fromPosition, toPosition)
        items[toPosition].listener.onEnableAutomaticUpdate(false)
        if (toPosition == 0) {
            action.invoke(items[toPosition].countryCode)
        }
    }

    override fun onItemDismissed(position: Int) {
        items.removeAt(position)
        notifyItemRemoved(position)
    }

    inner class ItemRateViewHolder(view: View) : RecyclerView.ViewHolder(view),
        ItemMovedHelperViewHolder {
        val itemRateView: ItemRateView = view as ItemRateView

        override fun onItemSelected() {
            itemRateView.setBackgroundColor(Color.LTGRAY)
        }

        override fun onItemClear() {
            itemRateView.setBackgroundColor(0)
        }
    }
}