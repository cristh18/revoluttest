package com.tolodev.revoluttest.ui.helpers

interface ItemMovedHelperAdapter {
    fun onItemMoved(fromPosition: Int, toPosition: Int)
    fun onItemDismissed(position: Int)
}