package com.tolodev.revoluttest.ui.activities

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.ItemTouchHelper
import com.google.android.material.snackbar.Snackbar
import com.tolodev.revoluttest.R
import com.tolodev.revoluttest.databinding.ActivityMainBinding
import com.tolodev.revoluttest.ui.adapters.RateAdapter
import com.tolodev.revoluttest.ui.helpers.ItemMovedHelperCallback
import com.tolodev.revoluttest.ui.viewModels.MainViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val viewModel by viewModel<MainViewModel>()

    private val rateAdapter: RateAdapter by lazy {
        RateAdapter {
            viewModel.getCurrenciesByReference(
                it
            )
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main)
        binding.lifecycleOwner = this
        setupView()
        subscribe()
    }

    private fun setupView() {
        val callBack = ItemMovedHelperCallback(rateAdapter)
        val touchHelper = ItemTouchHelper(callBack)
        binding.recyclerViewRates.apply {
            adapter = rateAdapter
            touchHelper.attachToRecyclerView(this)
            addItemDecoration(DividerItemDecoration(baseContext, DividerItemDecoration.VERTICAL))
            (itemAnimator as DefaultItemAnimator).supportsChangeAnimations = false
            (itemAnimator as DefaultItemAnimator).endAnimations()
        }
    }

    private fun subscribe() {
        viewModel.getHomeViewStateLiveData()
            .observe(this, Observer { binding.recyclerViewRates.post { rateAdapter.setItems(it) } })
        viewModel.showErrorEvent()
            .observe(this, Observer { showBottomMessage(it.message.toString()) })
    }

    private fun showBottomMessage(message: String): Snackbar {
        return Snackbar.make(findViewById(R.id.content_main), message, Snackbar.LENGTH_LONG)
    }
}
